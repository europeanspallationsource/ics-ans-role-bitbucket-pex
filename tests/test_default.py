import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_bitbucket_pex(Command):
    cmd = Command('/usr/local/bin/bitbucket.pex --version')
    assert cmd.stdout.strip() == 'bitbucket.pex, version 0.3.2'
